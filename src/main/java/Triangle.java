/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Triangle {
    private double base;
    private double height;
    public Triangle(double b,double h){
        this.base=b;
        this.height=h;
    }
    public double calArea(){
        return 0.5*base*height;
    }
    public double getB(){
        return base;
    }
    public double getH(){
        return height;
    }
    public void setLine(double b,double h){
        if(b<=0 || h<=0){
            System.out.println("Error: Base or Height must be more than zero");
            return;
        }
        this.base=b;
        this.height=h;
    }
}
