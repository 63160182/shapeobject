/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Rectangle {
    private double length;
    private double width;
    public Rectangle(double L,double W){
        this.length=L;
        this.width=W;
    }
    public double calArea(){
        return length*width;
    }
    public double getL(){
        return length;
    }
    public double getW(){
        return width;
    }
    public void setLine(double L,double W){
        if(L<=0 || W<=0){
            System.out.println("Error: Length or Width must be more than zero");
            return;
        }
        this.length=L;
        this.width=W;
    }
}
