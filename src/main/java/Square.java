/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Square {
    private double side;
    public Square(double s){
        this.side=s;
    }
    public double calArea(){
        return side*side;
    }
    public double getS(){
        return side;
    }
    public void setS(double s){
        if (s<=0){
            System.out.println("Error: Side length must be more than zero");
        }
        this.side=s;
    }
}
